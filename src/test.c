#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stddef.h> // For offsetof
#include <stdint.h> // For uint8_t

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void runTests() {
    printf("Starting tests...\n");

    //** Test 1 **//
    printf("Test 1\n");
    void* heap1 = heap_init(REGION_MIN_SIZE);
    assert(heap1 != NULL);
    void* content1 = _malloc(16);
    struct block_header* header1 = block_get_header(content1);
    assert(header1->is_free == false);
    assert(header1->capacity.bytes == 24);
    heap_term();

    //** Test 2 **//
    printf("Test 2\n");
    void* heap2 = heap_init(REGION_MIN_SIZE);
    assert(heap2 != NULL);
    void* content2_1 = _malloc(24);
    void* content2_2 = _malloc(100);
    assert(content2_1 != NULL && content2_2 != NULL);
    struct block_header* header2_1 = block_get_header(content2_1);
    struct block_header* header2_2 = block_get_header(content2_2);
    _free(content2_1);
    assert(header2_1->is_free == true);
    assert(header2_2->is_free == false);
    heap_term();

    //** Test 3 **//
    printf("Test 3\n");
    void* heap3 = heap_init(REGION_MIN_SIZE);
    assert(heap3 != NULL);
    void* content3_1 = _malloc(24);
    void* content3_2 = _malloc(24);
    void* content3_3 = _malloc(100);
    void* content3_4 = _malloc(25);
    assert(content3_1 != NULL && content3_2 != NULL && content3_3 != NULL && content3_4 != NULL);
    struct block_header* header3_1 = block_get_header(content3_1);
    struct block_header* header3_2 = block_get_header(content3_2);
    struct block_header* header3_3 = block_get_header(content3_3);
    struct block_header* header3_4 = block_get_header(content3_4);
    _free(content3_1);
    _free(content3_4);
    assert(header3_1->is_free == true);
    assert(header3_2->is_free == false);
    assert(header3_3->is_free == false);
    assert(header3_4->is_free == true);
    heap_term();

    //** Test 4 **//
    printf("Test 4\n");
    void* heap4 = heap_init(REGION_MIN_SIZE);
    assert(heap4 != NULL);
    void* content4 = _malloc(REGION_MIN_SIZE);
    assert(content4 != NULL);
    struct block_header* header4 = block_get_header(content4);
    assert(header4->capacity.bytes == REGION_MIN_SIZE);
    heap_term();

    //** Test 5 **//
    printf("Test 5\n");
    void* heap5 = heap_init(0);

    void* space = mmap(HEAP_START + REGION_MIN_SIZE, 24, 0, MAP_PRIVATE | 0x20, -1, 0);
    assert(heap5 != NULL && space != NULL);

    void* content5_1 = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    void* separate_content = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    assert(content5_1 != NULL && separate_content != NULL);
    struct block_header* separate_header = block_get_header(separate_content);
    munmap(space, 24);

    struct block_header* header5_1 = (struct block_header*) heap5;
    assert(header5_1->next == separate_header);
    assert((void*) header5_1 + offsetof(struct block_header, contents) + header5_1->capacity.bytes != separate_header);
    heap_term();

    printf("All tests completed successfully.\n");
}
