#ifndef LAB4_TEST_H
#define LAB4_TEST_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

void testOne();
void testTwo();
void testThree();
void testFour();
void testFive();

#endif //LAB4_TEST_H
